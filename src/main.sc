require: slotfilling/slotFilling.sc
    module = sys.zb-common

require: common.js
    module = common

require: city/cities-ru.csv
    name = Cities
    var = $Cities
    module = common
    
init:
    $global.$converters = {};
    $global.$converters.CityConverter = function CityConverter($parseTree) {var city_id = $parseTree.Cities[0].value;return $Cities[city_id].value.name;};
    
patterns: 
    $City = $entity<Cities> || converter = $converters.CityConverter
    
theme: /

    state: Start
        q!: ($regex</start>|* прив* *)
        a: Привет! Это приватный навык.
        a: Я погодный бот, рассказываю о погоде в разных городах мира.
        a: Погоду когда и в каком городе ты хочешь узнать?
    
    state: Help
        intent!: /Help
        a: Я погодный бот, рассказываю о погоде в разных городах мира.
        a: Назови город и на какой день нужен прогноз ))

    state: Stop
        intent!: /Stop
        random:
            a: Ок
            a: Хорошо

    state: GetWeather
        intent!: /GetWeather
        script:
            $session.Months = ['января', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']
        
            if (typeof $session.WeatherReport === "undefined") {
                //$reactions.answer("init WheatherReport");
                $session.WeatherReport = {
                    "cityname": "",
                    "lat": "",
                    "lon": "",
                    "temp": "",
                    "desc": "",
                    "date": "",
                    "when": "Сегодня"
                    };
            } else {
                //$reactions.answer("Change city or date");
            };
                
            if ("_cityname" in $parseTree) {
                $session.WeatherReport.cityname = $parseTree._cityname.name;
                //$reactions.answer("Set cityname - cityname");
            };
            if ("_forDay" in $parseTree) {
                var forday = new Date($parseTree._forDay.year, $parseTree._forDay.month - 1, $parseTree._forDay.day);
                $session.WeatherReport.date = forday.getTime();
                //$reactions.answer("Set date - forDay");
            };

            //$reactions.answer($session.WeatherReport.date);

            $temp.now = new Date();
            $temp.now.setHours(0,0,0,0);
            $temp.today = $temp.now.getDate();
            $temp.month = $temp.now.getMonth() + 1;
            
            if ($session.WeatherReport.date == "") {
                $session.WeatherReport.date = $temp.now.getTime();
            };
            
            // если запрос на другую дату
            var delta = $session.WeatherReport.date - $temp.now.getTime();
            $temp.delta = delta;
            $temp.report_date = new Date($session.WeatherReport.date);

            //$reactions.answer($temp.delta);
            //$reactions.answer($temp.now.getTime());
            //$reactions.answer($temp.today);
            //$reactions.answer($session.WeatherReport.date);

            // Get weather report for a week
            var url = "https://api.opencagedata.com/geocode/v1/json?limit=1&no_annotations=1&language=ru&q=" + $session.WeatherReport.cityname + "&key=" + $injector.opencage_apikey;;
            //$reactions.answer(url);
            
            var response = $http.get(url);
            if (response.isOk && response.data.results) {
                $session.WeatherReport.lat = response.data.results[0].geometry.lat; 
                $session.WeatherReport.lon = response.data.results[0].geometry.lng; 
            } else {
                $reactions.answer("Что-то пошло не так... не могу получить прогноз для города " + capitalize($nlp.inflect($session.WeatherReport.cityname, "nomn")) + "((");
            }; 
            
            var url = "https://api.openweathermap.org/data/2.5/onecall?appid=" + $injector.openweather_apikey + "&part=current,daily&units=metric&lang=ru&lat=" + $session.WeatherReport.lat + "&lon=" + $session.WeatherReport.lon;;
            //$reactions.answer(url);
            
            var response = $http.get(url);
            if (response.isOk) {
                if ($temp.delta == 0) {
                    // today forecast
                    $session.WeatherReport.temp = response.data.current.temp.toFixed(0);
                    $session.WeatherReport.desc = response.data.current.weather[0].description;
                } else {
                    // other day forecast
                    var items = response.data.daily;
                    for (var i = 0; i < items.length; i++) {
                        //$reactions.answer("Item " + i + " : " + items[i].dt);
                        //$reactions.answer("Report date: " + $session.WeatherReport.date);
                        var start_date = $session.WeatherReport.date/1000;
                        var end_date = start_date + 86400;
                        if (items[i].dt >= start_date && items[i].dt < end_date ) {
                            $session.WeatherReport.temp = items[i].temp.day.toFixed(0);
                            $session.WeatherReport.desc = items[i].weather[0].description;
                            //$reactions.answer(toPrettyString(items[i]));
                            break;
                        }
                    };
                    
                };
            } else {
                $reactions.answer("Что-то пошло не так... не могу получить прогноз ((");
            }; 
            
        if: ($temp.delta < 0)
            a: Погода в прошлом мне не доступна ((
            a: Спроси про погоду сегодня или завтра )) например 'Какая погода будет завтра в {{ capitalize($nlp.inflect($session.WeatherReport.cityname, "loc2")) }}?'
            go!: /GetWeather/NextWeek
        
        elseif: ($temp.delta > 7*86400000)
            a: Так далеко я не заглядываю )))
            a: Спроси про погоду сегодня или завтра )) например 'Какая погода будет завтра в {{ capitalize($nlp.inflect($session.WeatherReport.cityname, "loc2")) }}?'
            go!: /GetWeather/NextWeek
        
        elseif: ($temp.delta > 0 && $temp.delta <= 7*86400000)
            if: ($temp.delta == 86400000)
                script:
                    $session.WeatherReport.when = "Завтра";
            elseif: ($temp.delta == 2*86400000)
                script:
                    $session.WeatherReport.when = "Послезавтра";
            elseif: ($temp.delta == 7*86400000)
                script:
                    $session.WeatherReport.when = "Через неделю " + $temp.report_date.getDate() + " " + $nlp.inflect($session.Months[$temp.report_date.getMonth()], "gent");
            else:
                script:
                    $session.WeatherReport.when = $temp.report_date.getDate() + " " + $nlp.inflect($session.Months[$temp.report_date.getMonth()], "gent");
        else:
            script:
                $session.WeatherReport.when = "Сегодня";
        
        a: {{ $session.WeatherReport.when }} в {{ capitalize($nlp.inflect($session.WeatherReport.cityname, "loc2")) }} {{ $session.WeatherReport.desc }}. Температура воздуха {{$session.WeatherReport.temp}} {{ $nlp.conform("градус", $session.WeatherReport.temp)}}.
        
        state: OtherDay
            q: * @duckling.time::otherDay *
            script:
                var other_day = new Date($parseTree._otherDay.year, $parseTree._otherDay.month - 1, $parseTree._otherDay.day);
                $session.WeatherReport.date = other_day.getTime();
            go!: /GetWeather
        
        state: OtherCity
            q: * @MoreCities::otherCity *
            script:
                $session.WeatherReport.cityname = $parseTree._otherCity.name;
            go!: /GetWeather
            
        state: NextWeek
            a: В лучшем случае могу рассказать о погоде через неделю. Рассказать о погоде в {{ capitalize($nlp.inflect($session.WeatherReport.cityname, "loc2")) }} через неделю?
            
            state: NextWeekYes
                intent: /Yes
                script:
                    $temp.report_date = new Date();
                    $temp.report_date.setDate($temp.report_date.getDate() + 7);
                    var full = "";
                    // add zero before 1 digit day
                    if ($temp.report_date.getDate() < 9) {
                        full = '0' + $temp.report_date.getDate();
                        }
                    //$reactions.answer(full);
                    //$session.WeatherReport.when = "Через неделю " + full + " " + $nlp.conform($session.Months[$temp.report_date.getMonth()], $temp.report_date.getDate());
                    $session.WeatherReport.when = "Через неделю " + $temp.report_date.getDate() + " " + $nlp.inflect($session.Months[$temp.report_date.getMonth()], "gent");
                a: {{ $session.WeatherReport.when }} в {{ capitalize($nlp.inflect($session.WeatherReport.cityname, "loc2")) }} {{ $session.WeatherReport.desc }}. Температура воздуха {{$session.WeatherReport.temp}} {{ $nlp.conform("градус", $session.WeatherReport.temp)}}
                go!: /GoNext
            
            state: NextWeekNo
                intent: /No
                a: Хорошо )
                a: Погоду в каком городе ты хочешь узнать?
                go: /GetWeather

    state: GoNext
        a: Могу рассказать о погоде в любом другом городе
        go: /GetWeather
        
    state: Thanks
        intent!: /Thanks
        random: 
            a: Пожалуйста! 
            a: Обращайся еще :))
            a: Без проблем! )
        go: /GetWeather
            
    state: Error
        a: Извини, возникла проблема с получением данных. 
        a: Попробуй еще раз позже )
        go: /GetWeather
        
    state: NoMatch || noContext=true
        event!: noMatch
        a: Я не понял. Вы сказали: {{$request.query}}
